const usuSchemaDb = require("../models/usuario.model.js");
const bcryptjs = require("bcryptjs");

const getAllUsuario = async (req, res) => {
  const page = Number(req.query.page); // por parametro recibimos el parametro page y el param. limit
  const limit = Number(req.query.limit); // limit: me va a traer la cantidad de usuarios
  // skip: ignora todos los usuarios que estan a partir de este numero
  //page:
  const skipIndex = (page - 1) * limit;

  try {
    const count = await usuSchemaDb.countDocuments();
    const resultado = await usuSchemaDb
      .find()
      .sort({ _id: 1 })
      .limit(limit)
      .skip(skipIndex); // 1 ascendete(asc) 0 descendente(des)
    return res.status(200).json({
      code: "ok",
      message: null,
      success: true,
      data: {
        count,
        resultado,
      },
    });
  } catch (error) {
    return res.status(500).json({
      code: "ERROR",
      message: error.message,
      success: false,
      data: null,
    });
  }
};

const getUsuario = async (req, res) => {
  try {
    const usuario = await usuSchemaDb.findById(req.params._id);

    if (!usuario) {
      return res.status(404).json({
        code: "NOT - FOUND",
        message: null,
        success: false,
        data: null,
      });
    }
    return res.status(200).json({
      code: "ok",
      message: null,
      success: true,
      data: usuario,
    });
  } catch (error) {
    return res.status(500).json({
      code: "ERROR",
      message: error.message,
      success: false,
      data: null,
    });
  }
};

const postUsuario = async (req, res) => {
  const { password, ...resto } = req.body;
  resto.password = bcryptjs.hashSync(password, 10); //hasheamos. extragimos la pass del body cuando cargamos los datos de usuario nuevo
  // lo hasheamos y lo volvimos a meter en el resto
  try {
    const usuario = await usuSchemaDb.create(resto); //req.body

    return res.status(201).json({
      code: "ok",
      message: null,
      success: true,
      data: usuario,
    });
  } catch (error) {
    return res.status(500).json({
      code: "ERROR",
      message: error.message,
      success: false,
      data: null,
    });
  }
};

const putUsuario = async (req, res) => {
  try {
    const usuario = await usuSchemaDb.findOneAndUpdate(
      { _id: req.params._id },
      { ...req.body },
      { new: true }
    ); // esta tomando todos los atributos con sus valores del objeto que estoy pasando por el body y lo esta asignando al nuevo objeto
    //la funcion de arriba me va a devolver un usuario, que me lo va a mostrar en data

    if (!usuario) {
      return res.status(404).json({
        code: "NOT - FOUND",
        message: null,
        success: false,
        data: null,
      });
    }
    return res.status(200).json({
      code: "ok",
      message: null,
      success: true,
      data: usuario,
    });
  } catch (error) {
    return res.status(500).json({
      code: "ERROR",
      message: error.message,
      success: false,
      data: null,
    });
  }
};

const deleteUsuario = async (req, res) => {
  try {
    const usuario = await usuSchemaDb.deleteOne({ _id: req.params._id }); //valida que lo que yo quiero eliminar, tenga un id igual al id que le estoy pasando por parametro

    if (!usuario) {
      return res.status(404).json({
        code: "USUARIO A ELIMINAR INEXISTENTE",
        message: null,
        success: false,
        data: null,
      });
    }

    return res.status(200).json({
      code: "ok, usuario eliminado",
      message: null,
      success: true,
      data: null,
    });
  } catch (error) {
    return res.status(500).json({
      code: "Error, no se pudo eliminar usuario",
      message: error.message,
      success: false,
      data: null,
    });
  }
};

module.exports = {
  getAllUsuario,
  getUsuario,
  postUsuario,
  putUsuario,
  deleteUsuario,
};
