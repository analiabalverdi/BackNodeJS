const jwt = require ('jsonwebtoken');
const usuSchema = require ('../models/usuario.model.js');
const bcryptjs = require('bcryptjs');


const loginAuth = async (req, res) => {

const {email, password} =req.body;

    try {

        const usuario = await usuSchema.findOne({email});
        
        if(!usuario || !bcryptjs.compareSync(password, usuario.password)){
            return res.status (400).json({
                code:'AUTH-ERROR',
                message: 'Invalid Email or Password',
                sucess: false,
                data: null

        })
    };

    const token = jwt.sign({_id:usuario._id, rol:usuario.rol}, process.env.PRIVATE_KEY, { expiresIn: "8h" });
    
    return res.status (200).json({
            code:'OK',
            message: null,
            sucess: true,
            data: {usuario, token}
        });
        
    } catch (error) {
        return res.status (500).json({
            code:'error',
            message: error.message,
            sucess: false,
            data: null
    });

}
};

module.exports = {
    loginAuth
};
