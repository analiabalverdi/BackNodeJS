const ropaSchemaDb = require('../models/ropa.model.js');


const getAllRopa = async (req, res) => {

    const page = Number (req.query.page); // por parametro recibimos el parametro page y el param. limit  
    const limit = Number (req.query.limit);// limit: me va a traer la cantidad de usuarios
                                           // skip: ignora todos los usuarios que estan a partir de este numero
                                           //page:    
    const skipIndex = (page -1)* limit;
  
    try {
      const count = await ropaSchemaDb.countDocuments();
      const resultado = await ropaSchemaDb.find().sort({_id:1 }).limit(limit).skip(skipIndex)// 1 ascendete(asc) 0 descendente(des) 
       return res.status (200).json({
        code:"ok",
        message:null,
        success:true,
        data: {
          count,
          resultado
        }
  
       });
       } catch (error) {
        return res.status(500).json ({
          code: "ERROR",
          message: error.message,
          success:false,
          data: null
        });
   }
  };


const getRopa = async (req, res) => {
  
  try {
    const ropa = await ropaSchemaDb.findById(req.params._id);
   

    if (!ropa) {
      return res.status(404).json({
        code:"NOT - FOUND",
        message:null,
        success:false,
        data: null
       });
      };
     return res.status (200).json({
      code:"ok",
      message:null,
      success:true,
      data: ropa
    });


     
     } catch (error) {
      return res.status(500).json ({
        code: "ERROR",
        message: error.message,
        success:false,
        data: null
      });
 }};


const postRopa = async (req, res) => {
    
     try {
      const ropa = await ropaSchemaDb.create(req.body); //req.body
      
      
        return res.status(201).json({
         code:"ok",
         message:null,
         success:true,
         data: ropa
        });

     } catch (error) {
      return res.status(500).json ({
        code: "ERROR",
        message: error.message,
        success:false,
        data: null
      });
     }    
 };

 const putRopa = async (req, res) => {
   
  try { 
    const ropa = await ropaSchemaDb.findOneAndUpdate({_id: req.params._id}, {...req.body}, {new:true}); 
   
    if (!ropa) {
      return res.status(404).json({
        code:"NOT - FOUND",
        message:null,
        success:false,
        data: null
       });
      };
    return res.status (200).json({
      code:"ok",
      message:null,
      success:true,
      data: ropa
     });
     } catch (error) {
      return res.status(500).json ({
        code: "ERROR",
        message: error.message,
        success:false,
        data: null
      });
 }
};


const deleteRopa = async (req, res) => {
  try {
    const ropa = await ropaSchemaDb.deleteOne({_id: req.params._id});
    if (!ropa) {
      return res.status(404).json({
        code:"PRENDA INEXISTENTE",
        message:null,
        success:false,
        data: null
       });
      };
    
    return res.status (200).json({ 
      code:"ok, prenda eliminada",
      message:null,
      success:true,
      data: null
     });

    } catch (error) {
      return res.status(500).json ({
        code: "Error, no se pudo eliminar prenda",
        message: error.message,
        success:false,
        data: null
      });

 }
 };


 module.exports = {
    getAllRopa,
    getRopa,
    postRopa,
    putRopa,
    deleteRopa

 }