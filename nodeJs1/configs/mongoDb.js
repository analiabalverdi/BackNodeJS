
const mongoose = require ("mongoose");

const path = require ('path');
require('dotenv').config({path: __dirname + '/.env'});
const dbConecction = async () => {         //funcion asincrona de conect de mongoose el primer parametro es la direccion que nos da EL atlas 

    try { 
        await mongoose.connect(process.env.CONEXION_DB,{ // este es el link que nos dio mongo compas, pero lo hicimos variable de entorno en el .envairoment, como segundo parametro se le pasa una de las funciones de conect 
        useNewUrlParser: true,                            //esa funcion tiene un objeto con esas cuatro confguraciones 
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
  });
   console.log ("Db coenctada"); 
} catch (error) {
  console.log (error);
}


}

module.exports = dbConecction;