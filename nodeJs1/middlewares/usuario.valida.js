const Joi = require ('joi');

const validarUsuario = async (req, res, next) => {
  
    const schemaBody = Joi.object ({

      nombre: Joi.string().required(),
      apellido: Joi.string().required(),
      password: Joi.string().required(),
      email: Joi.string().email().required(),
      rol: Joi.string().valid("USER")}
    
  );
  
  try {
      await schemaBody.validateAsync(req.body); 
     next();
  } catch (error) {
    console.log (error)
      return res.status(400).json({
        code: "VALIDACION CON ERROR",
        message: error.details[0].message,
        success:false,
        data: null
        })
  }
};


const validarUsuarioMax = async (req, res, next) => {
  
  const schemaBody = Joi.object ({

    nombre: Joi.string().required(),
    apellido: Joi.string().required(),
    password: Joi.string().required(),
    email: Joi.string().email().required(),
    rol: Joi.string().valid("ADMIN","EMPLEADO","USUARIO")}
  
);

try {
    await schemaBody.validateAsync(req.body); 
   next();
} catch (error) {
  console.log (error)
    return res.status(400).json({
      code: "VALIDACION CON ERROR",
      message: error.details[0].message,
      success:false,
      data: null
      })
}
};


const valiRoles = (rol) => {
  return async (req,res,next) => {
      if(!rol.includes(req.usuario.rol)) {
          return res.status(403).json({
              code: "AUTH CON ERROR",
              message: "Acceso Restringido",
              success:false,
              data: null
              });
        }
      return next();

  };
  
}; 

  


    module.exports = { 
       validarUsuario,
       validarUsuarioMax,
       valiRoles
      };