const jwt = require ('jsonwebtoken');
const Joi = require ('joi');
const usuSchema = require ('../models/usuario.model');
const path = require ('path');
require('dotenv').config({path: __dirname + '/.env'});


const validaToken = async (req, res, next) => {

    if(req.url==="/auth/login" || req.url === "/usuario/crear") return next();
   
    
    const authorization = req.headers.authorization;
    if (!authorization){
        return res.status(401).json({                 
            code:'AUTH CON ERROR',
            message:"EL TOKEN DEBE SER PROVEIDO",
            success:false,
            data:null
                });
      }

     try {
       
    const token = authorization.split(" ")[1]; 
                                                            
    const { _id } = jwt.verify(token,process.env.PRIVATE_KEY); //vamos a guardar el id del payload, o sea del resultado del verify
                                                               // para que veamos si despues de haber tirado el token, el usuario aun existe para que haga gestiones 
    const usuario = await usuSchema.findById({_id});
    if(!usuario) {
       return res.status (401).json({
        code:'AUTH-ERROR',
        message: 'Usuario no existe',
        sucess: false,
        data: null

    })
};
 req.usuario = usuario;

 
    return next();
    
  } catch (error) {
    console.log(error.message);
    return res.status(400).json({
    code:'AUTH CON ERROR',
    message:error.message,
    success:false,
    data:null
    });
}
};

const validaLogin= async (req, res, next) => {    //VALIDA QUE LE PASE UN EMAIL Y PASSWORD
      
    const schemaBody = Joi.object({
      email: Joi.string().required(),
      password: Joi.string().required(),
    });
    try {
        await schemaBody.validateAsync(req.body); 
       next();
    } catch (error) {
        return res.status(400).json({
          code: "VALIDACION DE LOGIN CON ERROR",
          message: error.details[0].message,
          success:false,
          data: null
          })
    }
  };


module.exports= {
    validaToken,
    validaLogin,

}