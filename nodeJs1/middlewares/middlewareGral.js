const Joi = require ('joi');
const jwt = require ('jsonwebtoken');
const path = require ('path');
require('dotenv').config({path: __dirname + '/.env'});

const validarId = async (req, res, next) => {
      const schemaParams = Joi.object({
          _id: Joi.string().required()
      }); 
      
    
      try {
          await schemaParams.validateAsync(req.params); // Y EN ESTA LINEA ESTOY VALIDANDO LOS DATOS QUE VIENEN DEL REQ.PARAMS
          next();
      } catch (error) {
          return res.status(400).json({
            code: "ID INVALIDO",
            message: error.details[0].message, 
            success:false,
            data: null
            })
      }
    };


    const Middle = async (req, res, next) => {
    
        const _id = req.params._id
        
    const authorization = req.headers.authorization;
    
    try{
        
        const token = authorization.split(" ")[1]; 
        const usuario = await jwt.verify(token,process.env.PRIVATE_KEY);
        
        if(_id === usuario._id) {return next()}
           else {
               if(usuario.rol === "ADMIN") {return next()}
                 else  {
                    return res.status (401).json({
                     code:'AUTH-ERROR',
                     message: 'No tiene permiso para esta accion',
                     sucess: false,
                     data: null
             
                 })}
            };
        } catch (error){
            console.log(error.message);
            return res.status(400).json({
            code:'AUTH CON ERROR',
            message:error.message,
            success:false,
            data:null
            });
        }
    };
    module.exports = {
        validarId,
        Middle}