const Joi = require ('joi');
const jwt = require ('jsonwebtoken');
const path = require ('path');
require('dotenv').config({path: __dirname + '/.env'});



const validaRopa = async (req, res, next) => {
  
    const schemaBody = Joi.object ({

      tipo: Joi.string().required().valid("buzo","remera","campera","pantalon"),
      cantidad: Joi.number().max(10).required().positive(),
      precio: Joi.number().required().positive(),
      descripcion: Joi.string().required()}
    
  );
  
  try {
      await schemaBody.validateAsync(req.body); 
     next();
  } catch (error) {
    console.log (error)
      return res.status(400).json({
        code: "VALIDACION CON ERROR",
        message: error.details[0].message,
        success:false,
        data: null
        })
  }
};

const MiddleRopa = async (req, res, next) => {
    
    const _id = req.params._id
    
const authorization = req.headers.authorization;

try{
    
    const token = authorization.split(" ")[1]; 
    const usuario = await jwt.verify(token,process.env.PRIVATE_KEY);
    
    if(_id === usuario._id) {return next()}
       else {
           if(usuario.rol === "ADMIN" || usuario.rol === "EMPLEADO") {return next()}
             else  {
                return res.status (401).json({
                 code:'AUTH-ERROR',
                 message: 'No tiene permiso para esta accion',
                 sucess: false,
                 data: null
         
             })}
        };
    } catch (error){
        console.log(error.message);
        return res.status(400).json({
        code:'AUTH CON ERROR',
        message:error.message,
        success:false,
        data:null
        });
    }
};


module.exports = { 
    validaRopa,
    MiddleRopa
   };