const {Schema, model} = require("mongoose");
const bcryptjs = require('bcryptjs'); //para cifrar string aplicandole un algoritmo 


const usuSchema = Schema (
{

nombre: {
    type: String,
    required: true
},
apellido: {
    type: String,
    required:true
},
password:{
    type:String,
    required:true
},
email: {
    type:String,
    required: true,
    unique:true

},
rol: {
    type: String,
    required:true,
    enum: ["ADMIN","EMPLEADO","USER"]
}

},

    {
        timestamps:true
    },
       
);

usuSchema.methods.toJSON=function(){
    const {password, __v, ...usuario} = this.toObject()
    return usuario;
}

module.exports = model ("usuSchemaDb", usuSchema);