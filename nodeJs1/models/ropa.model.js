const {Schema, model} = require("mongoose");


const ropaSchema = Schema (
{

tipo: {
        type: String,
        required:true,
        enum: ["buzo","remera","campera","pantalon"]
    },
cantidad: {
    type: Number,
    required:true
},
precio:{
    type:Number,
    required:true
},
descripcion: {
    type:String,
    required: true,
}

},

    {
        timestamps:true
    },    
);

ropaSchema.methods.toJSON=function(){
    const {__v, ...ropa} = this.toObject()
    return ropa;
}


module.exports = model ("ropaSchemaDb", ropaSchema);