const express = require ('express');
const router = express.Router();

const {getAllRopa, getRopa, postRopa, putRopa, deleteRopa} = require ('../controllers/ropa.controll.js');

const {validarId, Middle} = require ('../middlewares/middlewareGral.js')
const {validaRopa,MiddleRopa} = require ('../middlewares/ropa.valida.js')



router.get('/', Middle, getAllRopa); //solo dueño del token o administrador para que vea toda la ropa

router.get('/:_id', validarId, getRopa);

router.post('/', MiddleRopa, validaRopa, postRopa);

router.put('/:_id', MiddleRopa, validaRopa, putRopa);

router.delete('/:_id', MiddleRopa, deleteRopa);


module.exports = router;  