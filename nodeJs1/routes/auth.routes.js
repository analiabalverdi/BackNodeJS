const express = require('express');
const router = express.Router();

const {loginAuth} = require ("../controllers/auth.controll.js");
const { validaLogin } = require ("../middlewares/auth.valida.js");



router.post ("/login", validaLogin, loginAuth);




module.exports = router; 