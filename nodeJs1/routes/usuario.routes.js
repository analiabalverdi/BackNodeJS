const express = require ('express');
const router = express.Router();

const {getAllUsuario, getUsuario, postUsuario, putUsuario, deleteUsuario} = require ('../controllers/usuario.controll.js');
const {validarUsuario,validarUsuarioMax,valiRoles} = require ('../middlewares/usuario.valida.js');
const {validarId, Middle} = require ('../middlewares/middlewareGral.js')




router.get('/', Middle, getAllUsuario); 
router.get('/:_id',validarId, Middle, getUsuario);

router.post('/crear', validarUsuario, postUsuario);//usuario q solo puede crear usuarios y sin token
router.post('/crearMax', valiRoles("ADMIN"),validarUsuarioMax, postUsuario)
//solo para admi que va a crear rol de admi o empleado con token (dado previamente)


router.put('/:_id', validarId, Middle, validarUsuario, putUsuario);
router.put('/max/:_id', validarId, valiRoles("ADMIN"), validarUsuarioMax, putUsuario);

router.delete('/:_id', validarId, Middle, deleteUsuario);


module.exports = router;  