const path = require ('path');
require('dotenv').config({path: __dirname + '/.env'});
const express = require("express");
const app = express();
const cors =require('cors'); // cuando una uri debe pegar en una uri distinta, existe el middleware cors para permitirle q acceda 
const morgan = require ('morgan');
const dbConecction = require('./configs/mongoDb.js');


app.use(morgan('tiny'));

//ROUTES
const usuarioRoutes = require ('./routes/usuario.routes.js');
const ropaRoutes = require ('./routes/ropa.routes');
const authRoutes = require ("./routes/auth.routes.js");

const {validaToken} =require("./middlewares/auth.valida.js")


app.use(express.text());
app.use(express.json());
app.use(cors());


app.use(validaToken);

//INSTANCIO RUTAS
app.use ('/usuario', usuarioRoutes);
app.use ('/ropa', ropaRoutes);
app.use ("/auth", authRoutes);





dbConecction();

const port = process.env.PORT;
app.listen(port, () => {
    console.log(`Server on port ${port}`);
});

